const express = require('express')
const app = express()
const port = 3000
const path = require('path');
const moment = require('moment'); // require

let article = {
  timer: new Date("2020-11-10 17:40:00")
}


app.get('/test', (req, res) => {
  res.sendFile(path.join(__dirname, './index.html'));
})

app.get('/end', (req, res) => {
  var now = moment();
  var endDate = moment(article.timer)
  var result = endDate.diff(now)
  res.send(result.toString());
})

app.get('/add_minutes', (req, res) => {
  var endDate = moment(article.timer)
  endDate=endDate.add(3,'minutes').add(0,'seconds').add(0,'milliseconds')
  article.timer=endDate;
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
